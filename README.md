# Setup
1. Scripts might not be allowed to run due to Microsoft strict execution policy. Open up the powershell in admin mode and run the following:

    - Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope CurrentUser
    - Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope LocalMachine

1. Now you can run the choco-install.ps1 script.
1. When that is done, then run the install-programs.ps1 script. (You may have to close the window to refresh the env variables)
1. When you are done, set the execution policies back to what they were before.

    - Set-ExecutionPolicy -ExecutionPolicy Undefined -Scope CurrentUser
    - Set-ExecutionPolicy -ExecutionPolicy Undefined -Scope LocalMachine
