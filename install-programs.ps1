choco feature enable -n allowGlobalConfirmation

# All programmes to download and install
# Maven install also comes the java JDK.
choco install maven -version 3.6.1
choco install python -version 3.7.3
choco install nodejs -version 12.4.0